﻿using Domain.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persistence.Context
{
    public class Contexto : DbContext
    {
        public Contexto(DbContextOptions<Contexto> options) : base(options)
        {
            //Database.EnsureDeleted();
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Cliente>().Ignore(d => d.Dividas);
            modelBuilder.Entity<DividaParcela>().Ignore(d => d.Divida);

            modelBuilder.SeedParametro();
            modelBuilder.SeedCliente();
        }

        public DbSet<Parametro> Parametro { get; set; }
        public DbSet<Divida> Divida { get; set; }
        public DbSet<DividaParcela> DividaParcela { get; set; }
        public DbSet<Cliente> Cliente { get; set; }

    }
}
