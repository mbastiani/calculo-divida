﻿using Domain.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persistence.Context
{
    public static class Extensoes
    {
        public static void SeedParametro
            (this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Parametro>()
                .HasData(
                    new Parametro
                    {
                        ParametroId = 1,
                        PercentualComissao = 10,
                        PorcentagemJuros = 0.2M,
                        QuantidadeMaxParcelas = 3,
                        TipoJuros = 1
                    }
                );
        }

        public static void SeedCliente
            (this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cliente>()
                .HasData(
                    new Cliente
                    {
                        ClienteId = 1,
                        Nome = "Cliente 1",
                        Telefone = "(11) 11111-1111"
                    },
                    new Cliente
                    {
                        ClienteId = 2,
                        Nome = "Cliente 2",
                        Telefone = "(22) 22222-2222"
                    },
                    new Cliente
                    {
                        ClienteId = 3,
                        Nome = "Cliente 3",
                        Telefone = "(33) 33333-3333"
                    },
                    new Cliente
                    {
                        ClienteId = 4,
                        Nome = "Cliente 4",
                        Telefone = "(33) 33333-3333"
                    }
                );
        }
    }
}
