﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Model
{
    public class DividaParcela
    {
        [Key]
        public int DividaParcelaId { get; set; }
        public DateTime DataVencimento { get; set; }
        public decimal ValorFinal { get; set; }

        [ForeignKey("Divida")]
        public int DividaId { get; set; }

        public virtual Divida Divida { get; set; }
    }
}
