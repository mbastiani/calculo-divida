﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain.Model
{
    public class Parametro
    {
        [Key]
        public int ParametroId { get; set; }

        [Required(ErrorMessage = "Quantidade máxima de parcelas é obrigatório.")]
        //[Range(1, int.MaxValue, ErrorMessage = "Quantidade máxima de parcelas deve ser maior que zero.")]
        public int QuantidadeMaxParcelas { get; set; }

        public short TipoJuros { get; set; }

        [Required(ErrorMessage = "Porcentagem de juros é obrigatório.")]
        //[Range(1, int.MaxValue, ErrorMessage = "Porcentagem de juros deve ser maior que zero.")]
        public decimal PorcentagemJuros { get; set; }

        [Required(ErrorMessage = "Porcentagem da comissão da Paschoalotto é obrigatório.")]
        //[Range(1, int.MaxValue, ErrorMessage = "Porcentagem da comissão da Paschoalotto deve ser maior que zero.")]
        public decimal PercentualComissao { get; set; }
    }
}
