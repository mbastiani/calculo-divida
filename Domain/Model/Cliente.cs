﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain.Model
{
    public class Cliente
    {
        [Key]
        public int ClienteId { get; set; }
        public string Nome { get; set; }

        public string Telefone { get; set; }
        public virtual IEnumerable<Divida> Dividas { get; set; }
    }
}
