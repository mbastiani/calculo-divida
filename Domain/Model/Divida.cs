﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Model
{
    public class Divida
    {
        [Key]
        public int DividaId { get; set; }
        public DateTime DataVencimento { get; set; }
        public DateTime DataCalculo { get; set; }
        public int QuantidadeParcelas { get; set; }
        public decimal ValorOriginal { get; set; }
        public decimal ValorComissao { get; set; }
        public int DiasAtraso
        {
            get
            {
                if (DataCalculo > DataVencimento)
                {
                    return (DataCalculo - DataVencimento).Days;
                }

                return 0;
            }
        }
        public decimal ValorJuros { get; set; }
        public decimal ValorFinal
        {
            get { return ValorOriginal + ValorJuros; }
        }
        public virtual IEnumerable<DividaParcela> Parcelas { get; set; }
        [ForeignKey("Cliente")]
        public int ClienteId { get; set; }
        public virtual Cliente Cliente { get; set; }

    }
}
