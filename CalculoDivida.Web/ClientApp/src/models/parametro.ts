export class Parametro {
    parametroId: number;
    quantidadeMaxParcelas: number;
    tipoJuros: number;
    porcentagemJuros: number;
    percentualComissao: number;
}
