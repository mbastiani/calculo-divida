import { Cliente } from './cliente';
import { DividaParcela } from './dividaParcela';

export class Divida {
    dividaId: number;
    dataVencimento: Date;
    dataCalculo: Date;
    quantidadeParcelas: number;
    valorOriginal: number;
    diasAtraso: number;
    valorJuros: number;
    valorFinal: number;
    parcelas: DividaParcela[];
    clienteId: number;
    cliente: Cliente;
}
