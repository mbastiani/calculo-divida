import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DividaService } from '../services/divida.service';
import { Divida } from 'src/models/divida';

@Component({
    selector: 'app-view-divida',
    templateUrl: './view-divida.component.html',
})
export class ViewDividaComponent {

    public divida: Divida;

    constructor(private _avRoute: ActivatedRoute, private _dividaService: DividaService, private _router: Router) {
        this.getDivida(this._avRoute.snapshot.params['id']);
    }

    getDivida(id) {
        this._dividaService.getDividaById(id).subscribe(
            (data: Divida) => this.divida = data
        );
    }

    back() {
        this._router.navigate(['/divida']);
    }
}
