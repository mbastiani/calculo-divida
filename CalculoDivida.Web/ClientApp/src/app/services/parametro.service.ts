import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Parametro } from 'src/models/parametro';

@Injectable({
    providedIn: 'root'
})
export class ParametroService {

    myAppUrl = '';

    constructor(private _http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
        this.myAppUrl = baseUrl;
    }

    getParametro() {
        return this._http.get(this.myAppUrl + 'api/Parametro')
            .pipe(map(
                response => {
                    return response;
                }));
    }

    updateParametro(parametro: Parametro) {
        return this._http.put(this.myAppUrl + 'api/Parametro', parametro)
            .pipe(map(
                response => {
                    return response;
                }));
    }
}
