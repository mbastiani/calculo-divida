import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Divida } from 'src/models/divida';

@Injectable({
    providedIn: 'root'
})
export class DividaService {

    myAppUrl = '';

    constructor(private _http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
        this.myAppUrl = baseUrl;
    }

    getDividas() {
        return this._http.get(this.myAppUrl + 'api/Divida')
            .pipe(map(
                response => {
                    return response;
                }));
    }

    getDividaById(dividaId:number) {
        return this._http.get(this.myAppUrl + 'api/Divida/' + dividaId)
            .pipe(map(
                response => {
                    return response;
                }));
    }

    insertDivida(divida: Divida) {
        return this._http.post(this.myAppUrl + 'api/Divida', divida)
            .pipe(map(
                response => {
                    return response;
                }));
    }

    updateDivida(divida: Divida) {
        return this._http.put(this.myAppUrl + 'api/Divida', divida)
            .pipe(map(
                response => {
                    return response;
                }));
    }

    deleteDivida(id: number) {
        return this._http.delete(this.myAppUrl + 'api/Divida/' + id)
            .pipe(map(
                response => {
                    return response;
                }));
    }
}
