import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ParametroService } from '../services/parametro.service';
import { Parametro } from 'src/models/parametro';

@Component({
    selector: 'app-edit-parametro',
    templateUrl: './edit-parametro.component.html',
})
export class EditParametroComponent implements OnInit {

    parametroForm: FormGroup;
    title = 'Editar';
    errorMessage: any;

    constructor(private _fb: FormBuilder, private _avRoute: ActivatedRoute,
        private _parametroService: ParametroService, private _router: Router) {

        this.parametroForm = this._fb.group({
            parametroId: 0,
            quantidadeMaxParcelas: ['', Validators.compose(
                [Validators.required, Validators.min(1)]
            )],
            tipoJuros: ['', [Validators.required]],
            porcentagemJuros: ['', Validators.compose(
                [Validators.required, Validators.min(0)]
            )],
            percentualComissao: ['', Validators.compose(
                [Validators.required, Validators.min(0)]
            )]
        })
    }

    ngOnInit() {
        this._parametroService.getParametro()
            .subscribe((response: Parametro) => {
                this.parametroForm.setValue(response);
            }, error => console.error(error));
    }

    save() {

        if (!this.parametroForm.valid) {
            return;
        }

        var parametro = new Parametro();

        parametro.parametroId = this.parametroForm.value.parametroId;
        parametro.quantidadeMaxParcelas = parseInt(this.parametroForm.value.quantidadeMaxParcelas);
        parametro.tipoJuros = parseInt(this.parametroForm.value.tipoJuros);
        parametro.porcentagemJuros = parseFloat(this.parametroForm.value.porcentagemJuros);
        parametro.percentualComissao = parseFloat(this.parametroForm.value.percentualComissao);

        this._parametroService.updateParametro(parametro)
            .subscribe(() => {
                this._router.navigate(['/']);
            }, error => console.log(error));
    }

    cancel() {
        this._router.navigate(['/']);
    }

    get quantidadeMaxParcelas() { return this.parametroForm.get('quantidadeMaxParcelas'); }
    get tipoJuros() { return this.parametroForm.get('tipoJuros'); }
    get porcentagemJuros() { return this.parametroForm.get('porcentagemJuros'); }
    get percentualComissao() { return this.parametroForm.get('percentualComissao'); }
}
