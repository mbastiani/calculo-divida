import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { EditParametroComponent } from './edit-parametro/edit-parametro.component';
import { EditDividaComponent } from './edit-divida/edit-divida.component';
import { FetchDividaComponent } from './fetch-divida/fetch-divida.component';
import { ViewDividaComponent } from './view-divida/view-divida.component';

@NgModule({
    declarations: [
        AppComponent,
        NavMenuComponent,
        HomeComponent,
        EditParametroComponent,
        EditDividaComponent,
        FetchDividaComponent,
        ViewDividaComponent
    ],
    imports: [
        BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forRoot([
            { path: '', component: HomeComponent, pathMatch: 'full' },
            { path: 'parametro', component: EditParametroComponent },
            { path: 'divida', component: FetchDividaComponent },
            { path: 'add-divida', component: EditDividaComponent },
            { path: 'divida/view/:id', component: ViewDividaComponent },
        ])
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
