import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ParametroService } from '../services/parametro.service';
import { ClienteService } from '../services/cliente.service';
import { DividaService } from '../services/divida.service';
import { Parametro } from 'src/models/parametro';
import { Cliente } from 'src/models/cliente';
import { Divida } from 'src/models/divida';

@Component({
    selector: 'app-edit-divida',
    templateUrl: './edit-divida.component.html',
})
export class EditDividaComponent implements OnInit {

    dividaForm: FormGroup;
    title = 'Inserir';
    errorMessage: any;
    dividaId: number;
    parametro: Parametro;
    clienteList: Cliente[];

    constructor(private _fb: FormBuilder, private _avRoute: ActivatedRoute,
        private _parametroService: ParametroService, private _clienteService: ClienteService, private _dividaService: DividaService, private _router: Router) {

        if (this._avRoute.snapshot.params['id']) {
            this.dividaId = this._avRoute.snapshot.params['id'];
        }
        
        this.dividaForm = this._fb.group({
            dividaId: 0,
            clienteId: ['', [Validators.required]],
            valorOriginal: ['', Validators.compose(
                [Validators.required, Validators.min(0)]
            )],
            quantidadeParcelas: ['', Validators.compose(
                [Validators.required, Validators.min(1)]
            )],
            dataVencimento: ['', [Validators.required]],
            dataCalculo: ['', [Validators.required]]
        })
    }

    ngOnInit() {
        this._clienteService.getClientes().subscribe(
            (data: Cliente[]) => this.clienteList = data
        );

        this._parametroService.getParametro().subscribe(
            (data: Parametro) => {
                this.dividaForm.controls["quantidadeParcelas"].setValidators([Validators.required, Validators.min(1), Validators.max(data.quantidadeMaxParcelas)]);
            });

        

        if (this.dividaId > 0) {
            this.title = 'Editar';
            this._dividaService.getDividaById(this.dividaId)
                .subscribe((response: Divida) => {
                    this.dividaForm.setValue(response);
                }, error => console.log(error));
        }
    }

    getParametro() {

    }

    save() {

        if (!this.dividaForm.valid) {
            return;
        }

        var divida = new Divida();

        divida.dividaId = this.dividaForm.value.dividaId;
        divida.clienteId = parseInt(this.dividaForm.value.clienteId);
        divida.valorOriginal = parseFloat(this.dividaForm.value.valorOriginal);
        divida.quantidadeParcelas = parseInt(this.dividaForm.value.quantidadeParcelas);
        divida.dataVencimento = this.dividaForm.value.dataVencimento;
        divida.dataCalculo = this.dividaForm.value.dataCalculo;

        if (this.title === 'Inserir') {
            this._dividaService.insertDivida(divida)
                .subscribe(() => {
                    this._router.navigate(['/divida']);
                }, error => console.log(error));
        }
        else {
            this._dividaService.updateDivida(divida)
                .subscribe(() => {
                    this._router.navigate(['/divida']);
                }, error => console.log(error));
        }
    }

    cancel() {
        this._router.navigate(['/']);
    }

    get clienteId() { return this.dividaForm.get('clienteId'); }
    get valorOriginal() { return this.dividaForm.get('valorOriginal'); }
    get quantidadeParcelas() { return this.dividaForm.get('quantidadeParcelas'); }
    get dataVencimento() { return this.dividaForm.get('dataVencimento'); }
    get dataCalculo() { return this.dividaForm.get('dataCalculo'); }
}
