import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DividaService } from '../services/divida.service';
import { Divida } from 'src/models/divida';

@Component({
    selector: 'app-fetch-divida',
    templateUrl: './fetch-divida.component.html',
})
export class FetchDividaComponent {

    public dividaList: Divida[];

    constructor(private _dividaService: DividaService) {
        this.getDividas();
    }

    getDividas() {
        this._dividaService.getDividas().subscribe(
            (data: Divida[]) => this.dividaList = data
        );
    }

    delete(id) {
        const ans = confirm('Deseja excluir a dívida?');
        if (ans) {
            this._dividaService.deleteDivida(id).subscribe(() => {
                this.getDividas();
            }, error => console.error(error));
        }
    }
}
