﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Domain.Model;
using Persistence.Context;

namespace CalculoDivida.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DividaController : ControllerBase
    {
        private readonly Contexto _context;

        public DividaController(Contexto context)
        {
            _context = context;
        }

        // GET: api/Divida
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Divida>>> GetDivida()
        {
            return await _context.Divida.Include(c => c.Cliente).ToListAsync();
        }

        // GET: api/Divida/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Divida>> GetDivida(int id)
        {
            var divida = await _context.Divida.Include(c => c.Cliente).Include(p => p.Parcelas).FirstOrDefaultAsync(x => x.DividaId == id);

            if (divida == null)
            {
                return NotFound();
            }

            return divida;
        }

        // PUT: api/Divida/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDivida(Divida divida)
        {
            _context.Entry(divida).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DividaExists(divida.DividaId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Divida
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Divida>> PostDivida(Divida divida)
        {
            var parametro = await _context.Parametro.FirstOrDefaultAsync();

            var taxaJuros = parametro.PorcentagemJuros / 100;

            if (parametro.TipoJuros == 1) // Juros Simples
            {
                divida.ValorJuros = divida.ValorOriginal * taxaJuros * divida.DiasAtraso;
            }
            else
            {
                var juros = Math.Pow(Convert.ToDouble(1 + taxaJuros), divida.DiasAtraso);
                divida.ValorJuros = Math.Round((divida.ValorOriginal * Convert.ToDecimal(juros)) - divida.ValorOriginal, 2);
            }

            divida.ValorComissao = Math.Round(divida.ValorFinal * (parametro.PercentualComissao / 100), 2);

            var valorParcela = Math.Round(divida.ValorFinal / divida.QuantidadeParcelas, 2);
            var dataParcela = divida.DataCalculo;
            var parcelas = new List<DividaParcela>();

            for (int i = 0; i < divida.QuantidadeParcelas; i++)
            {
                parcelas.Add(new DividaParcela
                {
                    DataVencimento = dataParcela,
                    ValorFinal = valorParcela,
                });

                dataParcela = dataParcela.AddMonths(1);
            }

            divida.Parcelas = parcelas;

            _context.Divida.Add(divida);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDivida", new { id = divida.DividaId }, divida);
        }

        // DELETE: api/Divida/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Divida>> DeleteDivida(int id)
        {
            var divida = await _context.Divida.FindAsync(id);
            if (divida == null)
            {
                return NotFound();
            }

            _context.Divida.Remove(divida);
            await _context.SaveChangesAsync();

            return divida;
        }

        private bool DividaExists(int id)
        {
            return _context.Divida.Any(e => e.DividaId == id);
        }
    }
}
