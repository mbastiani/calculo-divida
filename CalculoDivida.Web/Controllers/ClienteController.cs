﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Domain.Model;
using Persistence.Context;

namespace CalculoDivida.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClienteController : ControllerBase
    {
        private readonly Contexto _context;

        public ClienteController(Contexto context)
        {
            _context = context;
        }

        // GET: api/Cliente
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Cliente>>> GetCliente()
        {
            return await _context.Cliente.OrderBy(x => x.Nome).ToListAsync();
        }

        //// GET: api/Parametro
        //[HttpGet]
        //public async Task<ActionResult<Parametro>> GetParametro()
        //{
        //    var parametro = await _context.Parametro.FirstOrDefaultAsync();

        //    if (parametro == null)
        //    {
        //        parametro = new Parametro
        //        {
        //            PercentualComissao = 1,
        //            PorcentagemJuros = 1,
        //            QuantidadeMaxParcelas = 1,
        //            TipoJuros = 1
        //        };

        //        await _context.Parametro.AddAsync(parametro);
        //        await _context.SaveChangesAsync();

        //    }

        //    return parametro;
        //}
    }
}
