﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Domain.Model;
using Persistence.Context;

namespace CalculoDivida.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParametroController : ControllerBase
    {
        private readonly Contexto _context;

        public ParametroController(Contexto context)
        {
            _context = context;
        }

        // GET: api/Parametro
        [HttpGet]
        public async Task<ActionResult<Parametro>> GetParametro()
        {
            return await _context.Parametro.FirstOrDefaultAsync();
        }

        // PUT: api/Parametro/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut]
        public async Task<IActionResult> PutParametro(Parametro parametro)
        {
            _context.Entry(parametro).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ParametroExists(parametro.ParametroId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        private bool ParametroExists(int id)
        {
            return _context.Parametro.Any(e => e.ParametroId == id);
        }
    }
}
